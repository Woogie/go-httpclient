package examples

import (
	"net/http"
	"time"

	"gitlab.com/Woogie/go-httpclient/gohttp"
	"gitlab.com/Woogie/go-httpclient/gomime"
)

var (
	httpClient = getHttpClient()
)

func getHttpClient() gohttp.Client {
	headers := make(http.Header)
	headers.Set(gomime.HeaderContentType, gomime.ContentTypeJson)
	client := gohttp.NewBuilder().
		SetHeaders(headers).
		SetConnectionTimeout(2 * time.Second).
		SetResponseTimeout(3 * time.Second).
		SetUserAgent("Woogies-Computer").
		Build()
	return client
}
