package gohttp

import (
	"encoding/xml"
	"fmt"
//	"net/http"
	"testing"
)

func TestGetRequestBody(t *testing.T) {
	client:= httpClient{}

	t.Run("noBodyNilResponse",func(t *testing.T) {
		body, err := client.getRequestBody("", nil)

		if err != nil {
			t.Error("no error expected whn passing a nil body")
		}

		if err != nil {
			t.Error("no error expected whn passing a nil body")
		}

		if body != nil {
			t.Error("no body expected whn passing a nil body")
		}		
	})

	t.Run("BodyWithJson",func(t *testing.T) {
		requestBody := []string{"one", "two"}
		
		body, err := client.getRequestBody("application/json", requestBody)

		if err != nil {
			t.Error("no error expected when marshaling slice as json")
		}

		if string(body) != `["one","two"]` {
			t.Error("invalid json body obtained")
		}
	})

	t.Run("BodyWithXML",func(t *testing.T) {
		type Person struct {
			Name string
			Age  int
		}
		 
		requestBody := &Person{"Jack", 22}

		body, err := client.getRequestBody("application/xml", requestBody)

		if err != nil {
			t.Error("no error expected when marshaling slice as xml")
		}

		xml.Unmarshal([]byte(body), requestBody)

		if fmt.Sprint(requestBody) != `&{Jack 22}` {
			t.Error("invalid xml body obtained")
		}
		
	})

	t.Run("BodyWithJsonAsDefault",func(t *testing.T) {
		requestBody := []string{"one", "two"}
		
		body, err := client.getRequestBody("application/json", requestBody)

		if err != nil {
			t.Error("no error expected when marshaling slice as json")
		}

		if string(body) != `["one","two"]` {
			t.Error("invalid json boy obtained")
		}		
	})


}